<?php
/**
* Plugin Name : City Weather Report
* Description : Display Weather Report in Widget Area
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

/**
* API Endpoint Example
* http://api.wunderground.com/api/6a71941176e55332/geolookup/conditions/q/CA/San_Francisco.json
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load scripts
require_once(plugin_dir_path(_FILE_).'/includes/city-weather-report-script.php');

// Load Geoplugin
require_once(plugin_dir_path(_FILE_).'/includes/geoplugin-class.php');

// Load class
require_once(plugin_dir_path(_FILE_).'/includes/city-weather-report-class.php');

// Register Widgets
function register_city_weather_report(){
	register_widget('City_Weather_Report_Widget');
}

add_action('widget_init','register_city_weather_report');
?>
<?php
/**
* Plugin Name : Facebook Footer Link
* Description : Add facebook profile under the posts
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Global Options Variable
$ffl_options = get_option('ffl_settings');

// Load scripts
require_once(plugin_dir_path(_FILE_).'/includes/facebook-footer-link-script.php');

// Load content
require_once(plugin_dir_path(_FILE_).'/includes/facebook-footer-link-content.php');

// Load settings
if(is_admin()){
	require_once(plugin_dir_path(_FILE_).'/includes/facebook-footer-link-settings.php');
}

?>
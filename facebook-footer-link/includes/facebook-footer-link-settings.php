<?php
// Create menu links
function ffl_options_menu_link(){
	add_options_page(
	'Facebook Footer Link Options',
	'Facebook Footer Link',
	'manage_options',
	'ffl-options',
	'ffl_options_content'
	);
}
// Create Option Page Content
function ffl_options_content(){
ob_start();
?>
	<div class="wrap">
    	<h2><?php _e('Facebook Footer Link Settings', 'ffl-domain') ?></h2>
        <p><?php _e('Settings for Facebook Footer Link Plugin', 'ffl-domain') ?></p>
        <form method="post" action="options.php">
        	<?php settings_fields('ffl_setting_groups'); ?>
            <table>
            	<tbody>
                	<tr>
                    	<th scope="row"><label for="ffl_settings[enable]"><?php _e('Enable', 'ffl-domain'); ?></label></th>
                        <td><input name="ffl_settings[enable]" type="checkbox" id="ffl_settings[enable]" value="1"  <?php checked('1', $ffl_options['enable']); ?> /></td>
                    </tr>
                    <tr>
                    	<th scope="row"><label for="ffl_settings[facebook_url]"><?php _e('Facebook Profile URL', 'ffl-domain'); ?></label></th>
                        <td><input name="ffl_settings[facebook_url]" type="text" id="ffl_settings[facebook_url]" value="<?php echo $ffl_options['facebook_url']; ?>" class="regular-text" />
                        <p><?php _e('Enter facebook profile URL', 'ffl-domain'); ?></p>
                        </td>
                    </tr>
                    <tr>
                    	<th scope="row"><label for="ffl_settings[link_color]"><?php _e('Link Color', 'ffl-domain'); ?></label></th>
                        <td><input name="ffl_settings[link_color]" type="text" id="ffl_settings[link_color]" value="<?php echo $ffl_options['link_color']; ?>" class="regular-text" />
                        <p><?php _e('Enter color or HEX with a #', 'ffl-domain'); ?></p>
                        </td>
                    </tr>
                    <tr>
                    	<th scope="row"><label for="ffl_settings[show_in_feeds]"><?php _e('Show in Posts Feeds', 'ffl-domain'); ?></label></th>
                        <td><input name="ffl_settings[show_in_feeds]" type="checkbox" id="ffl_settings[show_in_feeds]" value="1"  <?php checked('1', $ffl_options['show_in_feeds']); ?> /></td>
                    </tr>
                </tbody>
            </table>
            <p class="submit"><input type="submit" id="submit" class="button button-primary" value="<?php _e('Save Changes', 'ffl-domain'); ?>" /></p>
        </form>
    </div>
<?php
echo ob_get_clean();
}

add_action('admin_menu', 'ffl_options_menu_link');

// Register Settings
function ffl_register_settings(){
	register_setting('ffl_setting_groups', 'ffl_settings');
}

add_action('admin_init','ffl_register_settings');
?>
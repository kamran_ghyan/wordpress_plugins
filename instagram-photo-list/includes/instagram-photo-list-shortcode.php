<?php
// List Photos
function ipl_list_phots($atts, $content = null){
	global $ipl_options;
	// Create Atributes And Defaults
	$atts = shortcode_atts(array(
		'title' 	=> 'Instagram photos list',
		'count' 	=> 20
	), $atts);
	
	$url = 'https://api.instagram.com/v1/users/self/?access_token='. $ipl_options['access_token'] . '&count=' . $atts['count'];
	$options = array('http' => array('user_agent' => $_SERVER['HTTP_USER_AGENT']));
	$context = stream_context_create($options);
	$response = file_get_contents($url,false,$context);
	$data = json_decode($response)->$data;
	
	
		
	// Init Output
	$output = '';
	
	// Build Output
	$output .= '<div class="ipl-photos">';
	foreach($data as $photo){
		
		$output .= '<div class="photo-col">';
		if($ipl_options['linked']){
			$output .= '<a target="_blank" title="'.$photo->caption->text.'" href="'.$photo->link.'"><img src="'.$photo->images->standard_resolution->url.'" /></a>';
		}else {
			$output .= '<img src="'.$photo->images->standard_resolution->url.'" />';
		}
		
		$output .= '</div>';
	}
	
	$output .= '</div>';
		
}

// Todo List Shortcode
add_shorcode('photos', 'ipl_list_photos');
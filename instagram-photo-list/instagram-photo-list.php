<?php
/**
* Plugin Name : Instagram Photo List
* Description : Show latest Instagram photos
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Global Options Variables
$ipl_options = get_option('ipl_settings');

// Load Scripts
require_once(plugin_dir_path(_FILE_).'/includes/instagram-photo-list-script.php');

// Load Shortcode
require_once(plugin_dir_path(_FILE_).'/includes/instagram-photo-list-shortcode.php');

if(is_admin()){
			
	// Load Setting Page
	require_once(plugin_dir_path(_FILE_).'/includes/instagram-photo-list-settings.php');
	
}
// Register Widgets
function register_newsletter_subscriber(){
	register_widget('Newsletter_Subscriber_Widget');
}

add_action('widget_init','register_newsletter_subscriber');
?>
<?php

/*
*	Wordpress Github Repos Class
*/

class WP_My_Github_Repos_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'my_github_repos_widget', // Base ID
			__( 'My Github repos', 'mgr_domain' ), // Name
			array( 'description' => __( 'Gihub Repository Widget', 'mgr_domain' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
	
		// get Values
		$title = apply_filters('widget_title', $instance['title']);
		$username = esc_attr($instance['username']);
		$count = esc_attr($instance['count']);
		
		// outputs the content of the widget
		echo $args['before_widget'];
		echo $args['before_title'];
		if(!empty($instance['title'])){
			echo $instance['title'];
		}
		echo $args['after_title'];
		
		echo $this->showRepos($title, $username, $count);
		
		echo $args['after_widget'];
		
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		
		// Get Tile
		if(isset($instance['title'])){
			$title = $instance['title'];
		} else{
			$title = __('Latest Github Repos', 'mgr_domain');
		}
		
		// Get Username
		if(isset($instance['username'])){
			$username = $instance['username'];
		} else{
			$username = __('bradtraversy', 'mgr_domain');
		}
		
		// Get Count
		if(isset($instance['count'])){
			$count = $instance['count'];
		} else{
			$count = 5;
		}
		?>
        	<p>
            	<label for="<?php echo get_field_id('title'); ?>"><?php _e('Title', 'mgr_domain'); ?></label>
                <input type="text" class="widefat" id="<?php echo get_field_id('ttile'); ?>" name="<?php echo get_field_name('title'); ?>" value="<?php echo esc_html($title); ?>" />
            </p>
            
            <p>
            	<label for="<?php echo get_field_id('username'); ?>"><?php _e('Username', 'mgr_domain'); ?></label>
                <input type="text" class="widefat" id="<?php echo get_field_id('username'); ?>" name="<?php echo get_field_name('username'); ?>" value="<?php echo esc_html($username); ?>" />
            </p>
            
            <p>
            	<label for="<?php echo get_field_id('count'); ?>"><?php _e('Count', 'mgr_domain'); ?></label>
                <input type="text" class="widefat" id="<?php echo get_field_id('count'); ?>" name="<?php echo get_field_name('count'); ?>" value="<?php echo esc_html($count); ?>" />
            </p>
            
        <?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array(
			'title' => (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '',
			'username' => (!empty($new_instance['username'])) ? strip_tags($new_instance['username']) : '',
			'count' => (!empty($new_instance['count'])) ? strip_tags($new_instance['count']) : ''
		);
		return $instance;
	}
	
	// Show Repsitories
	
	public function showRepos($title, $username, $count){
		
		$url = 'http://api.github.com/users/'.$username.'/repos?sort=create&per_page='.$count;
		$options = array(
			'http' => array(
				'user_agent' => $_SERVER['HTTP_USER_AGENT']
			)
		);
		$context = stream_context_create($options);
		$response = file_get_contents($url, false, $context);
		$repos = json_decode($response);
		
		// Build Output
		$output = '<ul class="respos">';
		
		foreach($repos as $repo){
			$output .= '<li>
					   	<div class="repo-title">'.$repo->name.'</div>
						<div class="repo-desc">'.$repo->description.'</div>
						<a target="_blank" href="'.$repo->html_url.'">View on Github</div>
			';
		}
		
		$output .= '</ul>';
		
		return $output;
			
	}
	
}
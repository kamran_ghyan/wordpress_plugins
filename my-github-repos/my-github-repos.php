<?php
/**
* Plugin Name : My Github Repos
* Description : Display user github repos in widget
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load Scripts
require_once(plugin_dir_path(_FILE_).'/includes/my-github-repos-script.php');


// Load Class
require_once(plugin_dir_path(_FILE_).'/includes/my-github-repos-class.php');



// Register Widgets
function mgr_register_widget(){
	register_widget('Newsletter_Subscriber_Widget');
}

add_action('widget_init','mgr_register_widget');
?>
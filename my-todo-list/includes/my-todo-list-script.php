<?php

// Check If Admin
if(is_admin()){
	// Add Admin Scripts
	function mtl_add_admin_scripts(){
		wp_enqueue_style('mtl-admin-style', plugins_url().'/my-todo-list/css/style-admin.css' );
	}
	
	add_action('admin_init','mtl_add_admin_scripts');
}

// Add Admin Scripts
function mtl_add_scripts(){
	wp_enqueue_style('mtl-main-style', plugins_url().'/my-todo-list/css/style.css' );
	wp_enqueue_script('mtl-main-script', plugins_url().'/my-todo-list/js/main.js', 'jquery' );
}

add_action('wp_enqueue_scripts','mtl_add_scripts');

?>
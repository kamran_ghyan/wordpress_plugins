<?php
// List Todos
function mtl_list_todo($atts, $content = null){
	global $post;
	// Create Atributes And Defaults
	$atts = shortcode_atts(array(
		'title' 	=> 'My Todo',
		'count' 	=> 10,
		'category' => 'all'
	), $atts);
	
	// Check Category Attributes
	if($atts['categoy'] == 'all' ){
		$terms = '';
	} else {
		$terms = array(
			array(
				'taxonomy' 	=> 'category',
				'field' 	=> 'slug',
				'terms' => $atts['category']
		));
	}
	
	// Query Args
	$args = array(
		'post_type' 	=> 'todo',
		'post_status' 	=> 'publish',
		'orderby' 		=> 'due_date',
		'order' 		=> 'ASC',
		'post_per_page' => $atts['count'],
		'tax_query' 	=> $terms
	);
	
	// Fetch Todo
	$todos = new WP_Query($args);
	
	// Check Fot Todos
	if($todos->have_posts()){
		
		// Get Category Slug
		$category = str_replace('-','', $atts['category']);
		$category = strtolower($atts['category']);
		
		// Init Output
		$output = '';
		
		// Build Output
		$output .= '<div class="todo-list">';
		while($todos->have_posts()){
			$todos->the_post();
			
			// Get Fields Values
			$priority = get_post_meta($post->ID, 'priority', true);
			$details = get_post_meta($post->ID, 'details', true);
			$due_date = get_post_meta($post->ID, 'due_date', true);
			
			$output .= '<div class="todo">';
			$output .= '<h4>'.get_the_title().'</h4>';
			$output .= '<div>'.$details.'</div>';
			$output .= '<div class="priority-'.strtolower($priority).'">Priority:'.$priority.'</div>';
			$output .= '<div class="due_date">Due Date: '.$due_date.'</div>';
			$output .= '</div>';
		}
		
		$output .= '</div>';
		
		// Reset Post Data
		wp_reset_postdata();
		
		return $output;
	} else {
		return 'No Todos';
	}
}

// Todo List Shortcode
add_shorcode('todo', 'mtl_list_todos');
<?php
/**
* Plugin Name : My Todo List
* Description : My todo list Plugin
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load Scripts
require_once(plugin_dir_path(_FILE_).'/includes/my-todo-list-script.php');


// Load Shortcode
require_once(plugin_dir_path(_FILE_).'/includes/my-todo-list-shortcode.php');



if(is_admin()){
	
	// Load Custom Post Type
	require_once(plugin_dir_path(_FILE_).'/includes/my-todo-list-cpt.php');
	
	// Load Field
	require_once(plugin_dir_path(_FILE_).'/includes/my-todo-list-fields.php');
	
}
// Register Widgets
function register_newsletter_subscriber(){
	register_widget('Newsletter_Subscriber_Widget');
}

add_action('widget_init','register_newsletter_subscriber');
?>
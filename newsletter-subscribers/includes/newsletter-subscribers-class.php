<?php
	
	class Newsletter_Subscriber_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		parent::__construct(
			'newsletter_subscriber_widget', // Base ID
			__( 'Newsletter Subscriber Widget', 'ns_domain' ), // Name
			array( 'description' => __( 'A simple email subscriber', 'ns_domain' ), ) // Args
		);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		
		// outputs the content of the widget
		echo $args['before_widget'];
		echo $args['before_title'];
		if(!empty($instance['title'])){
			echo $instance['title'];
		}
		echo $args['after_widget'];
	?>
    	<h3><?php ?></h3>
        <div id="form-msg"></div>
        <form id="subscriber-form" method="post" action="<?php echo plugin_url() . '/newsletter-subscribers/includes/newsletter-subscribers-mailer.php'; ?>">
        	
            <div id="form-group">
            	<label for="name">Name: </label><br />
                <input type="text" id="name" name="name" class="form-control" required />
            </div>
            
            <div id="form-group">
            	<label for="email">Email: </label><br />
                <input type="text" id="email" name="email" class="form-control" required />
            </div>
            <br />
            <input type="text" id="recipient" name="recipient" class="form-control" value="<?php echo $instance['recipient']; ?>" />
            <input type="text" id="subject" name="subject" class="form-control" value="<?php echo $instance['subject']; ?>" />
            <input type="submit" class="btn btn-primary" name="subscriber_name" value="Subscribe" />
            <br /><br />
        </form>
    <?php
	echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin
		$this->getForm( $instance );
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = array(
			'title' => (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '',
			'recipient' => (!empty($new_instance['recipient'])) ? strip_tags($new_instance['recipient']) : '',
			'subject' => (!empty($new_instance['subject'])) ? strip_tags($new_instance['subject']) : ''
		);
		return $instance;
	}
	
	
	/**
	 * Get Form for Newsletter Subscriber
	 *
	 * @param array $instance The widget options
	 */
	public function getForm( $instance ) {
		
		$title = !empty($instance['title']) ? strip_tags($new_instance['title']) : __('Newsletter Subscriber', 'ns_domain');
		$recipient = $instance['recipient'];
		$subject = !empty($instance['subject']) ? strip_tags($new_instance['subject']) : __('Newsletter Subscriber', 'ns_domain');
		
		?>
            <p>
            	<label for="<?php echo $this->get_field_id('title'); ?>" ><?php _e('Title:') ?></label><br />
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            
            <p>
            	<label for="<?php echo $this->get_field_id('recipient'); ?>" ><?php _e('Recipient:') ?></label><br />
                <input class="widefat" id="<?php echo $this->get_field_id('recipient'); ?>" name="<?php echo $this->get_field_name('recipient'); ?>" type="text" value="<?php echo esc_attr($recipient); ?>" />
            </p>
            
            <p>
            	<label for="<?php echo $this->get_field_id('subject'); ?>" ><?php _e('Subject:') ?></label><br />
                <input class="widefat" id="<?php echo $this->get_field_id('subject'); ?>" name="<?php echo $this->get_field_name('subject'); ?>" type="text" value="<?php echo esc_attr($subject); ?>" />
            </p>
            
            
        <?php
	}
	
	
	
}
	
?>
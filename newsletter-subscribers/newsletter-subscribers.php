<?php
/**
* Plugin Name : Newsletter Subsciber
* Description : A form to subscribe to a newsletter
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load scripts
require_once(plugin_dir_path(_FILE_).'/includes/newsletter-subscribers-script.php');

// Load class
require_once(plugin_dir_path(_FILE_).'/includes/newsletter-subscribers-class.php');

// Register Widgets
function register_newsletter_subscriber(){
	register_widget('Newsletter_Subscriber_Widget');
}

add_action('widget_init','register_newsletter_subscriber');
?>
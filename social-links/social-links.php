<?php
/**
* Plugin Name : Social Links
* Description : Add socail icons links with profile
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.kamranghyan.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load scripts
require_once(plugin_dir_path(_FILE_).'/includes/social-links-script.php');

// Load class
require_once(plugin_dir_path(_FILE_).'/includes/social-links-class.php');

// Register Widgets
function register_social_links(){
	register_widget('Social_Links_Widget');
}

add_action('widget_init','register_social_links');
?>
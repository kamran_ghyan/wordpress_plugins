<?php

function yvg_setting_api_init(){
	add_settings_section(
		'yvg_setting_section',
		'Youtube Video Gallery',
		'yvg_setting_section_callback',
		'reading'
	);
	
	add_settings_field(
		'yvg_setting_disable_fullscreen',
		'Disable Fullscreen',
		'yvg_setting_disable_fullscreen_callback',
		'reading',
		'yvg_setting_section'
	);
	
	register_setting('reading', 'yvg_setting_disable_fullscreen');
}

add_action('admin_init', 'yvg_setting_api_init');

function yvg_setting_section_callback(){
	echo 'Settings for Youtube Vid Gallery';
}

function yvg_setting_disable_fullscreen_callback(){
	echo '<input type="checkbox" name="yvg_setting_disable_fullscreen" id="yvg_setting_disable_fullscreen" class="code" value="1"'.checked(1,get_option('yvg_setting_disable_fullscreen',false));
}
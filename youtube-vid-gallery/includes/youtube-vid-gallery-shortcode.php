<?php
// List Todos
function yvg_list_videos($atts, $content = null){
	global $post;
	// Create Atributes And Defaults
	$atts = shortcode_atts(array(
		'title' 	=> 'Video Gallery',
		'count' 	=> 20,
		'category' => 'all'
	), $atts);
	
	// Check Category Attributes
	if($atts['categoy'] == 'all' ){
		$terms = '';
	} else {
		$terms = array(
			array(
				'taxonomy' 	=> 'category',
				'field' 	=> 'slug',
				'terms' => $atts['category']
		));
	}
	
	// Query Args
	$args = array(
		'post_type' 	=> 'video',
		'post_status' 	=> 'publish',
		'orderby' 		=> 'due_date',
		'order' 		=> 'ASC',
		'post_per_page' => $atts['count'],
		'tax_query' 	=> $terms
	);
	
	// Fetch Todo
	$videos = new WP_Query($args);
	
	// Check Fot Todos
	if($videos->have_posts()){
		
		// Get Category Slug
		$category = str_replace('-','', $atts['category']);
		$category = strtolower($atts['category']);
		
		// Init Output
		$output = '';
		
		// Build Output
		$output .= '<div class="video-list">';
		while($videos->have_posts()){
			$videos->the_post();
			
			// Get Fields Values
			$video_id = get_post_meta($post->ID, 'video_id', true);
			$details = get_post_meta($post->ID, 'details', true);
			
			$output .= '<div class="yvg-video">';
			$output .= '<h4>'.get_the_title().'</h4>';
			if(get_settings('yvg_setting_disable_fullscreen')){
				$output .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$video_id.'" frameborder="0"></iframe>';
			} else{
				$output .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$video_id.'" frameborder="0" allowfullscreen></iframe>';
			}
			
			$output .= '<div>'.$details.'</div>';
			$output .= '</div><br /><hr>';
		}
		
		$output .= '</div>';
		
		// Reset Post Data
		wp_reset_postdata();
		
		return $output;
	} else {
		return 'No Videos Found';
	}
}

// Todo List Shortcode
add_shorcode('videos', 'yvg_list_videos');
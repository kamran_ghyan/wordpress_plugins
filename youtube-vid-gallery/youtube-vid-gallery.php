<?php
/**
* Plugin Name : Youtube Video Gallery
* Description : Add a Youtube Video Gallery to your Website
* Version : 1.0
* Author : Kamran Ghyan
* URI : http://www.allshoreresources.net
**/

// Exit if directly access directory
if(!defined('ABSPATH')){
	exit;
}

// Load Scripts
require_once(plugin_dir_path(_FILE_).'/includes/youtube-vid-gallery-script.php');


// Load Shortcode
require_once(plugin_dir_path(_FILE_).'/includes/youtube-vid-gallery-shortcode.php');



if(is_admin()){
	
	// Load Custom Post Type
	require_once(plugin_dir_path(_FILE_).'/includes/youtube-vid-gallery-cpt.php');
	
	// Load Field
	require_once(plugin_dir_path(_FILE_).'/includes/youtube-vid-gallery-fields.php');
	
	// Load Settings
	require_once(plugin_dir_path(_FILE_).'/includes/youtube-vid-gallery-settings.php');
	
}
// Register Widgets
function register_newsletter_subscriber(){
	register_widget('Newsletter_Subscriber_Widget');
}

add_action('widget_init','register_newsletter_subscriber');
?>